// console.log('Hello World');

// [SECTION] Objects

/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
    - Different data types may be stored in an object's property creating complex data structures
*/

// Creating objects using object initializers/literal notation

/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/

let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}
let laptop = new Laptop("Lenovo", 2008)
console.log('Result from creating objects using object constructors');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020);
console.log('Result from creating objects using object constructors');
console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMS', 1980);
console.log('Result from creating objects without the new keyword');
console.log(oldLaptop); // undefined
// make sure to use 'new' keyword to store data / values successfully;

// Creating empty objects
let computer = {};
// let myComputer = new Objects();


// [SECTION] Accessing Object Properties
// using dot notation

console.log('Result from dot notation: ' + myLaptop.name);
console.log('Result from dot notation: ' + cellphone.name);

// Using the square bracket notation
console.log ('Result from square bracket notation: ' + myLaptop['name']);
console.log('Result from square bracket notation: ' + cellphone['name'])